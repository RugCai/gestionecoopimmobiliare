drop database if exists GestioneCoopImmobiliare;
create database GestioneCoopImmobiliare;
use GestioneCoopImmobiliare;

create table immobili (
	idImmobile int,
    tipo varchar(1),
    city varchar(255)
	);

create table locatari (
	idLocatario int,
    nominativo varchar(255),
    constraint pk_idLocatario primary key(idLocatario)
	);

create table affitti (
	idAffitto int,
    ratax varchar(255),
    durata int not null,
    idImmobile int,
    idLocatario int,
    constraint pk_idAffitto primary key(idAffitto),
    constraint fk_idImmobile foreign key(idImmobile) references immobili(idImmobile),
    constraint idLocatario foreign key(idLocatario) references locatari(idLocatario)
	);

create table spese (
	idSpesa int, 
    importo int not null,
    dataSpesa date,
    constraint pk_idSpesa primary key(idSpesa),
    constraint fk_idAffitto foreign key(idAffitto) references affitti(idAffitto)
    );

-- insert
insert into immobili(idImmobile, tipo, city) values (1,A,'varese'),(2,N,'como'),(3,U,'milano'),(4,A,'varese'),(5,A,'milano'),(6,U,'varese'),(7,A,'como');
insert into locatari(idLocatario, nominativo) values (1,'rossi'),(2,'verdi'),(3,'bianchi'),(4,'neri');
insert into affitti(idAffitto, ratax, durata, idImmobile, idLocatario) values(1,1,5,2,1),(2,3,3,3,1),(3,0,2,4,2),(4,2,2,5,2),(5,1,1,6,3),(6,1,3,7,4),(7,0,2,7,4);
insert into spese(idSpesa, importo, dataSpesa, idAffitto) values (1,3000,'01/04/2007',1),(2,5000,'01/07/2007',1),(3,1500,'14/02/2009',5),(4,4500,'03/05/2008',7),(5,875,'15/01/2009',7);

-- viste 

create view ContrattoMaggiore2Anni as 
	Select l.nominativo
    from affitti as a
    join locatori as l on l.idLocatario = a.idLocatario 
    where a.durata > 2;

create view NumeroImmobiliAffittatiPerCittà as
	Select Count(i.idImmobile), i.city
    from affitti as A
    join immobili as i on i.idImmobile = a.idImmobile
    group by i.city;

create view MancatoPagamento3Rate as
	Select *
    from affitti as a 
    where a.ratax > 3;

create view SpeseRossi2007 as 
	Select s.importo, s.data
    from spese as s
    join affitti as a on s.idAffitto = a.idAffitto
    join location as l on a.idLocatario = l.idLocatario
    where a.nominativo = 'rossi' and s.data between '01/01/2007' and '31/12/2007';
    
create view LavoriImportoMaggiore1000 as
	Select s.data
    from spese as s
    join affitti as a on s.idAffitto = a.idAffitto
    join locatari as l on a.idLocatario = l.idlocatario
    where s.importo> 1000 and l.nominativo = 'bianchi';
    
create view SpeseAnnualiPerCittà as 
	Select i.city, SUM(s.importo)
    from spese as s
    join affitti as a on s.idAffitto = a.idAffitto
    join immobili as i on a.idImmobili = i.idImmobili
    where s.data between '01/01/2007' and '31/12/2007'
    group by i.city;

create view TipiImmobili as
	Select count(*), i.tipo
    from immobili as i
    group by i.tipo;
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    